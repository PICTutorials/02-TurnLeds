;   TurnLED's - Turning 3 LED's
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;==============================================
;TurnLED's (2009 - Steven Rodriguez)
;==============================================

;General description
;=============================
; This program will turn on the led on W:
; 0 = nothing
; 1 = red led (RA0)
; 2 = green led (RA1)
; 3 = yellow led (RA2)
;=============================

;General Purpose Registers (BE CAREFUL BITCH!!! :P)
;====================================================
LEDSTATUS EQU 0x21
SWSTATUS EQU 0x22

;Assembler directives
;=============================
	list p=16f628a
	include p16f628a.inc
	__config b'11111100001001'
	org 0x00
	goto Start

;Functions
;=============================
ChangeBank0
	bcf STATUS,RP1 ; Go to bank 0
        bcf STATUS,RP0
	return
ChangeBank1
	bcf STATUS,RP1 ; Go to bank 1
        bsf STATUS,RP0
	return

;Program
;=============================
Start
	goto Initialize
Initialize
	call ChangeBank0 ; Go to bank 0
	clrf PORTA ; Clear PORTA register
	clrf PORTB ; Clear PORTB register
	call ChangeBank1 ; Go to bank 1
	bcf TRISA,0 ; Make RA0 output (0 = output, 1 = input) 
	bcf TRISA,1 ; Make RA1 output
	bcf TRISA,2 ; Make RA2 output
        bsf TRISB,0 ; Make RB0 input
	call ChangeBank0 ; Go to bank 0
	movlw .0 ; Load decimal 0 to W (all leds off)
	movwf LEDSTATUS ; Move value from W to LEDSTATUS
	movlw .0 ; Load decimal 0 to W (all leds off)
        movwf SWSTATUS ; Move value from W to SWSTATUS
	goto Cycle
Cycle
	btfss PORTB,0 ; Check RB0 input
	goto TurnSwOn
	goto TurnSwOff	
TurnSwOn
	movf SWSTATUS,0 ; Move SWSTATUS TO W
        sublw .0 ; Check if SWSTATUS is 0
        btfsc STATUS,Z ; Check if Z
	incf LEDSTATUS,1 ; Increment LEDSTATUS
	movlw .1 ; Turn SWSTATUS to 1
	movwf SWSTATUS
	goto CheckLedStatus
TurnSwOff
	movlw .0 ; Turn SWSTATUS to 0
	movwf SWSTATUS	
	goto CheckLedStatus
TurnRedLed
        bsf PORTA,0
        bcf PORTA,1
        bcf PORTA,2
        goto Cycle
TurnGreenLed
        bcf PORTA,0
        bsf PORTA,1
        bcf PORTA,2
        goto Cycle
TurnYellowLed
        bcf PORTA,0
        bcf PORTA,1
        bsf PORTA,2
        goto Cycle
TurnOffLeds
        bcf PORTA,0
        bcf PORTA,1
        bcf PORTA,2
        goto Cycle
ResetLeds
        bcf PORTA,0
        bcf PORTA,1
        bcf PORTA,2
        movlw .0 ; Load decimal 0 to W (all leds off)
        movwf LEDSTATUS ; Move value from W to LEDSTATUS
        goto Cycle
CheckLedStatus
        movf LEDSTATUS,0 ; Move LEDSTATUS TO W
        sublw .0 ; Check if LEDSTATUS is 0
        btfsc STATUS,Z ; Check if Z
        goto TurnOffLeds
        movf LEDSTATUS,0 ; Move LEDSTATUS TO W
        sublw .1 ; Check if LEDSTATUS is 1
        btfsc STATUS,Z ; Check if Z
        goto TurnRedLed
        movf LEDSTATUS,0 ; Move LEDSTATUS TO W
        sublw .2 ; Check if LEDSTATUS is 2
        btfsc STATUS,Z ; Check if Z
        goto TurnGreenLed
	movf LEDSTATUS,0 ; Move LEDSTATUS TO W
        sublw .3 ; Check if LEDSTATUS is 3
        btfsc STATUS,Z ; Check if Z
        goto TurnYellowLed
        movf LEDSTATUS,0 ; Move LEDSTATUS TO W
        sublw .4 ; Check if LEDSTATUS is 4
        btfsc STATUS,Z ; Check if Z
        goto ResetLeds
        goto Cycle
	end
